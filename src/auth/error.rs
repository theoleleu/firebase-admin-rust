use custom_error::custom_error;

custom_error! {pub KeyError
    Unreacheable{source: reqwest::Error} = "Firebase certificates unreacheable ({source}).",
    Invalid{pem_error: pem::PemError} = "Invalid Firebase key ({pem_error}).",
    NotParsable = "Unable to parse Firebase certificate.",
    KeyNotFound = "Unable to find key in Firebase certificate.",
    NotFound = "The certificate linked to this token cannot be found.",
}

custom_error! {pub AuthError
    TokenDecodeError{jwt_error: jsonwebtoken::errors::Error} = "The provided token cannot be decoded ({jwt_error}).",
    InternalKeyError{source: KeyError} = "Retrieving the key failed ({source}).",
    TokenHeaderMalformed = "The provided token is malformed.",
    TokenNotYetValid = "The provided token is not yet valid.",
    UnableToEncodeCustomToken{jwt_error: jsonwebtoken::errors::Error} = "Unable to encode custom token ({jwt_error})."
}

// PemError is not standard, we need to convert it manually
impl From<pem::PemError> for KeyError {
    fn from(error: pem::PemError) -> Self {
        KeyError::Invalid { pem_error: error }
    }
}
