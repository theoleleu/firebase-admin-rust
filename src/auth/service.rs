use super::error::{AuthError, KeyError};
use crate::app::App;
use chrono::Utc;
use der_parser::*;
use jsonwebtoken::{decode, decode_header, encode, Algorithm, Header, Validation};
use nom::IResult;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use url::Url;
use futures::TryFutureExt;

const ID_TOKEN_CERT_URI: &str =
    "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com";
const FIREBASE_AUDIENCE: &str =
    "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit";
const ONE_HOUR_IN_SECONDS: i64 = 60 * 60;

#[derive(Hash, Eq, PartialEq, Serialize, Deserialize, Debug)]
pub enum SignInProvider {
    #[serde(rename = "google.com")]
    Google,
    #[serde(rename = "facebook.com")]
    Facebook,
    #[serde(rename = "firebase")]
    Firebase,
    #[serde(rename = "github.com")]
    Github,
    #[serde(rename = "phone")]
    Phone,
    #[serde(rename = "playgames.google.com")]
    PlayGames,
    #[serde(rename = "twitter.com")]
    Twitter,
    #[serde(rename = "email")]
    Email,
    #[serde(rename = "password")]
    Password,
}

#[serde(default)]
#[derive(Default, Serialize, Deserialize, Debug)]
pub struct FirebaseClaims {
    pub identities: HashMap<SignInProvider, Vec<String>>,
    pub sign_in_provider: Option<SignInProvider>,
}

#[serde(default)]
#[derive(Default, Serialize, Deserialize, Debug)]
pub struct DecodedIdToken {
    #[serde(with = "url_serde")]
    pub picture: Option<Url>,
    #[serde(with = "url_serde")]
    iss: Option<Url>,
    aud: String,
    auth_time: i64,
    user_id: String,
    pub sub: String,
    iat: i64,
    exp: i64,
    pub email: Option<String>,
    pub email_verified: bool,
    pub firebase: FirebaseClaims,
}

/// Our claims struct, it needs to derive `Serialize` and/or `Deserialize`
#[derive(Debug, Serialize, Deserialize)]
struct CustomClaims {
    aud: String,
    iat: i64,
    exp: i64,
    iss: String,
    sub: String,
    uid: String,
}

/*
 * Test for a the sequence that contains the key (ie: modulus + exponent)
 */
fn rsa_key_parser(seq: &[u8]) -> IResult<&[u8], DerObject> {
    parse_der_sequence_defined!(seq, parse_der_integer, parse_der_integer)
}

/*
 * The DER-encoded public key is stored within a bitstring embedded either
 * in a certificate or an x.509 public key.
 *
 * To find the bitstring we need to sift through the DER field sequences
 * recursively.
 */
fn rec_extract_key(sequence: &Vec<DerObject>) -> Option<Vec<u8>> {
    for o in sequence {
        match &o.content {
            der_parser::DerObjectContent::BitString(_, bso) => {
                match rsa_key_parser(bso.data) {
                    Ok(_) => {
                        let mut key: Vec<u8> = Vec::new();
                        key.extend_from_slice(bso.data);

                        return Some(key);
                    }
                    Err(_) => (),
                };
            },
            der_parser::DerObjectContent::OctetString(oso) => {
                let mut key: Vec<u8> = Vec::new();
                key.extend_from_slice(oso);

                return Some(key);
            }
            der_parser::DerObjectContent::Sequence(s) => match rec_extract_key(s) {
                Some(k) => return Some(k),
                None => (),
            },
            _ => (),
        }
    }

    return None;
}

/*
 * Extract a DER-formatted key from either a certificate or an x.509 public key
 */
fn extract_key(cert: &[u8]) -> Result<Vec<u8>, KeyError> {
    let result = parse_der(cert);
    let parsed = match result {
        Ok(p) => p,
        _ => return Err(KeyError::NotParsable),
    };

    let (_, der_object) = parsed;
    match der_object.content {
        der_parser::DerObjectContent::Sequence(s) => {
            Ok(rec_extract_key(&s).ok_or(KeyError::KeyNotFound)?)
        }
        _ => Err(KeyError::KeyNotFound),
    }
}

async fn public_certificates() -> Result<DecodedIdToken, Box<(dyn std::error::Error + 'static)>> {
        let res = reqwest::r#Client::new()
            .get(ID_TOKEN_CERT_URI)
            .send();
        res.json()
            .map_err(|err| err.into()).await
}

async fn cert_der(key_id: String) -> Result<DecodedIdToken, Box<(dyn std::error::Error + 'static)>> {
    let certs = public_certificates();
    move |certs| {
        let cert = certs.get(&key_id).ok_or(KeyError::NotFound)?;
        let cert_der = pem::parse(cert)?;
        Ok(extract_key(cert_der.contents.as_slice())?)
    }
}

async fn key_der(private_key_pem: String) -> Result<Vec<u8>, KeyError> {
    let parsed_key = pem::parse(private_key_pem).map(|key| key.contents)?;
    extract_key(parsed_key.as_slice()).map_err(|error| error.into())
}

pub async fn decode_firebase_token(
    app: App,
    token: String,
) -> Result<DecodedIdToken, Box<(dyn std::error::Error + 'static)>> {
            let token = Ok(token.clone()).unwrap();

            // Find public key id in token header
            let header = decode_header(&token)
                .map_err(|error| AuthError::TokenDecodeError { jwt_error: error })?;
            
            let kid = header.kid.ok_or(AuthError::TokenHeaderMalformed).unwrap();
            
            // Find corresponding public key in Google data
            cert_der(kid).map_err(|err| err.into());
            move |cert_der| {
                // Prepare key validation rules
                let mut validation = Validation::new(Algorithm::RS256);
                validation.set_audience(&[app.project_id]);
                validation.validate_exp = true;
                validation.iss = Some(format!(
                    "https://securetoken.google.com/{}",
                    app.project_id.clone()
                ));
                let decoded_token =
                    decode::<DecodedIdToken>(token.as_str(), &cert_der, &validation)
                        .map_err(|error| AuthError::TokenDecodeError { jwt_error: error })?;

                // Perform complementary validation as specified by Fiebase documentation
                // let now = Utc::now().timestamp();
                // if decoded_token.claims.auth_time >= now {
                //     return Err(AuthError::TokenNotYetValid);
                // }

                // Return claims
                Ok(decoded_token.claims)
            }
            .map_err(|err| err.into())
}

pub fn create_custom_token(app: App, uid: String) -> Result<String, AuthError> {
    // Prepare header
    let mut header = Header::default();
    header.alg = Algorithm::RS256;
    header.typ = Some(String::from("JWT"));

    // Prepare claims
    let now = Utc::now().timestamp();
    let claims = CustomClaims {
        aud: String::from(FIREBASE_AUDIENCE),
        iat: now,
        exp: now + ONE_HOUR_IN_SECONDS,
        iss: app.credential.client_email.clone(),
        sub: app.credential.client_email.clone(),
        uid,
    };

    // Prepare secret key
    let private_key = key_der(app.credential.private_key.clone());

    // Encode JWT
    encode(&header, &claims, private_key.as_slice())
        .map_err(|error| AuthError::UnableToEncodeCustomToken { jwt_error: error })
}
