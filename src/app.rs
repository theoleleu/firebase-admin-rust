use super::credentials::{load_certificate, Certificate};

#[derive(Debug, Clone)]
pub struct App {
    pub name: String,
    pub credential: Certificate,
    pub project_id: String,
}

pub fn initialize_app(path: String, name: String) -> App {
    let certificate = load_certificate(path);
    let project_id = certificate.project_id.clone();

    return App {
        name: name,
        credential: certificate,
        project_id: project_id,
    };
}
