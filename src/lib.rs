extern crate chrono;
extern crate custom_error;
extern crate der_parser;
extern crate futures;
#[macro_use]
extern crate nom;
#[macro_use]
extern crate rusticata_macros;
extern crate serde;
extern crate serde_json;
extern crate url;
extern crate url_serde;

// Common modules
pub mod app;
pub mod auth;
pub mod credentials;
pub mod messaging;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
